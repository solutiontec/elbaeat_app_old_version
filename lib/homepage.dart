import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HomePage extends StatefulWidget{
  final BuildContext context;
  final String websiteUrl;
  HomePage(this.context, this.websiteUrl );
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomePage();
  }
}

class _HomePage extends State<HomePage> {

  // Override della funziona init (Cosi esegue getJsonData all'inizio)
  @override
  void initState() {
    super.initState();
    // this.getJsonData(); // get menu items
    // this.setUpWebsiteUrl();

  }
  /** Menu JSON
  final String url = "https://crm.solutiontec.it/api/app/ostiascacchi/menu";
  var drawerOptions = <Widget>[];
  String menuIcon = "";
  String menuTitle = "";
  String menuSubtitle = "";
  String logo = "";

  // Funzione ASYNC per ottenere il risultato Json e parsato
  Future<String> getJsonData() async {
    var response = await http.get(
      // Encoide url
        Uri.encodeFull(url),
        // Accettiamo solo Json
        headers: {"Accept": "application/json"}
    );
    setState(() {
      var json_decoded = jsonDecode(response.body);
      menuIcon = json_decoded['menuIcon'];
      menuTitle = json_decoded['menuTitle'];
      menuSubtitle = json_decoded['menuSubtitle'];
      // _website.add(json_decoded['website']);
      //listSite.add(json_decoded['website']);
      //sito_web = json_decoded['website'];
      // widget.website = json_decoded['website'];
      logo = json_decoded['logo'];
      for (var i = 0; i < json_decoded['menu'].length; i++) {
        drawerOptions.add(
            new ListTile(
                leading: new Icon(Icons.arrow_forward_ios),
                title: new Text(json_decoded['menu'][i]['name']),
                onTap: () =>
                    _navPage(context, json_decoded['menu'][i]['name'],
                        json_decoded['menu'][i]['url'])
            )
        );
      }
    });
    return "Success";
  }**/
  Completer<WebViewController> _controller = Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {

    // _website.add(this.sito.toString());
    // TODO: implement build
    return Scaffold(
        /*
        appBar: AppBar(
          title: Text(
              'Benvenuto',
              style: TextStyle(color: Colors.white)
          ),
          backgroundColor: Color(0xFF4035b1),
        ), */
        /*drawer: Drawer(
          child: new Column(
            children: <Widget>[
              new UserAccountsDrawerHeader(
                  accountName: Text(
                      menuTitle),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                            Color(0xFF4268D3),
                            Color(0xFF584CD1)
                          ],
                          begin: FractionalOffset(0.2, 0.0),
                          end: FractionalOffset(1.0, 0.6),
                          stops: [0.0, 0.6],
                          tileMode: TileMode.clamp
                      )
                  ),
                  accountEmail: Text(menuSubtitle),
                  currentAccountPicture: new CircleAvatar(
                    radius: 50.0,
                    backgroundColor: const Color(0xFF778899),
                    backgroundImage: NetworkImage(logo),
                  )
              ),
              new Column( children: drawerOptions )
            ],
          ),
        ),*/
        body: Builder(builder: (BuildContext context) {
          print('SIAMO IN HOMEPAGE');
          print(widget.websiteUrl);
          return WebView(
            initialUrl: widget.websiteUrl,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
            gestureNavigationEnabled: true,
          );
        }),
      // floatingActionButton: favoriteButton(),
    );//new Page("home", "http://www.ostiascacchi.it/"));

  }

  /** Push Page  **/
  void _navPage(BuildContext context, title, url) {
    Navigator.push(
        context, new MaterialPageRoute(builder: (context) => new Page(title, url)));
  }
}

class Page extends StatelessWidget{
  final String titleText;
  final String urlSource;
  Page(this.titleText, this.urlSource);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new WebviewScaffold(
      url: urlSource,
      appBar: new AppBar(
        title: Text(titleText),
        backgroundColor: Color(0xfffcf750),
      ),
      withZoom: true,
      withLocalStorage: true,
      hidden: true,
    );
  }
}





