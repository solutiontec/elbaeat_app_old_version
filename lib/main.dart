import 'package:flutter/material.dart';
import 'package:app_2/homepage.dart';
import 'package:package_info/package_info.dart';
import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main() => runApp(
    MaterialApp(
      home: MyApp(),
    )
);

class MyApp extends StatefulWidget {
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
    super.initState();
    this.launchInitApp();
  }
  // Get information from API
  String currentVersion = "";
  String versionApp = "";
  String websiteUrl = "";
  Future<String> launchInitApp() async {
    // get this version
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    // get menu json
    var response = await http.get(
        // Encoide url
        Uri.encodeFull("https://crm.solutiontec.it/api/app/elbaeat/menu"),
        // Accettiamo solo Json
        headers: {"Accept": "application/json"}
    );
    // Set state
    setState(() {
      // Current Version
      currentVersion = packageInfo.version;
      // Version app
      var json_decoded = jsonDecode(response.body);
      versionApp = json_decoded['version']; // version app
      websiteUrl = json_decoded['website']; // url website
    });
    // if version is updated show alert
    if ( currentVersion != versionApp ) {
      await showDialog<void>(
          context: context,
          barrierDismissible: true, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('La tua versione è obsoleta'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Text("Aggiorna l'ultima versione disponibile " + versionApp )
                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: ()=> exit(0),
                ),
              ],
            );
          }
      );
    } else {
      // Redirect to website only if updated to last version
      Future.delayed(
          Duration(seconds: 3),
              () {
            Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => HomePage(context, websiteUrl),
            ));
          }
      );
    }
    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: const Color(0xfffcf750),
      ),
      child: Center(
        child: Image.network(
            'https://crm.solutiontec.it/images/elbeat-logo.png',
            // width: 300,
            height: 100 ,
            width: 300,
            fit:BoxFit.fill
        ),
      ),
    );
  }
}


